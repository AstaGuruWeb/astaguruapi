﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class GetBidrecordbyProductIdController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;
        public HttpResponseMessage Get(int productid)
        {
            List<Bidrecord> objaucList = new List<Bidrecord>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec spGetBidRecords " + productid + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Bidrecord objAuction = new Bidrecord();

                    objAuction.Firstname = dr["Firstname"].ToString();
                    objAuction.Lastname = dr["Lastname"].ToString();
                    objAuction.Thumbnail = dr["Thumbnail"].ToString();
                    objAuction.Bidpricers = dr["Bidpricers"].ToString();
                    objAuction.Bidpriceus = dr["Bidpriceus"].ToString();
                    objAuction.Username = dr["Username"].ToString();
                    objAuction.daterec = dr["daterec"].ToString();

                    objAuction.UserId = dr["UserId"].ToString();
                    objAuction.Bidrecordid = dr["Bidrecordid"].ToString();
                    objAuction.validbidpricers = dr["validbidpricers"].ToString();
                    objAuction.validbidpriceus = dr["validbidpriceus"].ToString();
                    objAuction.currentbid = dr["currentbid"].ToString();
                    objAuction.recentbid = dr["recentbid"].ToString();
                    objAuction.anoname = dr["anoname"].ToString();

                    objAuction.productid = dr["productid"].ToString();
                    objAuction.Auctionid = dr["Auctionid"].ToString();

                    objAuction.proxy = dr["proxy"].ToString();


                    objaucList.Add(objAuction);

                }
                //response = Request.CreateResponse(HttpStatusCode.OK, objaucList);
            }
            //else
            //{
            //    string Message = "There is no bid history";
            //    response = Request.CreateResponse(HttpStatusCode.OK, Message);
            //}

            response = Request.CreateResponse(HttpStatusCode.OK, objaucList);
            return response;

        }
    }
}
