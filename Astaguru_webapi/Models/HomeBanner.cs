﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class HomeBanner
    {
        public int homebannerID { get; set; }

        public string homebannerTitle { get; set; }


        public string homebannerImg { get; set; }

        public int type { get; set; }

        public int bannerType { get; set; }
        public string dateAdded { get; set; }
        public int auctionId { get; set; }
        public string auctionPageUrl { get; set; }

        public string urlID { get; set; }

        public string Auctionname { get; set; }

        public string versionAndroid { get; set; }

        public string versionIOS { get; set; }

        public string webImages { get; set; }

        public string livecount { get; set; }

    }
}