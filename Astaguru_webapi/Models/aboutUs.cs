﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class aboutUs
    {
        public int aboutId { get; set; }

        public string about { get; set; }

        public int status { get; set; }


    }

    public class resourceaboutUs
    {
        public List<aboutUs> resource { get; set; }
    }
}