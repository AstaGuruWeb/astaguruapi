﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class AddToGallaryController : ApiController
    {
        HttpResponseMessage response;
        Utility util = new Utility();
        addtogallaryResponse objmessage = new addtogallaryResponse();
        public HttpResponseMessage Get(int productID,int userId)
        {
   
            DataTable dt = new DataTable();
            try
            {

                using (SqlCommand cmd = new SqlCommand("spAddToGallery"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@productID", productID);
                    cmd.Parameters.AddWithValue("@siteUserID", userId);



                    if (util.Execute(cmd))
                    {


                        objmessage.Message = "Lot Successfully added to Gallary";
                        response = Request.CreateResponse(HttpStatusCode.OK, objmessage);

                    }
                    else
                    {
                        objmessage.Message = "Lot Successfully added to Gallary";
                        response = Request.CreateResponse(HttpStatusCode.OK, objmessage);
                    }
                }

            }
            catch (Exception ex)
            {
                objmessage.Message = "Something went please try after Sometime";
                response = Request.CreateResponse(HttpStatusCode.BadRequest, objmessage);
            }


            return response;


        }
    }
}
