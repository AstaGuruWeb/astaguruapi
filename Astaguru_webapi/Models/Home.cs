﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Home
    {

        public int homebannerID { get; set; }
        public string homebannerTitle { get; set; }
        public string homebannerImg { get; set; }
        public int type { get; set; }
        public int bannerType { get; set; }
        public int auctionId { get; set; }
        public string auctionPageUrl { get; set; }
        public int urlID { get; set; }
        public string webImages { get; set; }
        public string Auctionname { get; set; }

         public string auctionsName { get; set; }

         public string recentAuctionBanner { get; set; }

        
        
    }
}
