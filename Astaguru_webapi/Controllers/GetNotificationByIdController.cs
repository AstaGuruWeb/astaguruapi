﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class GetNotificationByIdController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;

        public HttpResponseMessage Get(int userid)
        {
            List<Pushnotification> objnotificationlist = new List<Pushnotification>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_PushNotification 'GetNotoficationByuserId',"+ userid + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Pushnotification objnotification = new Pushnotification();
                    objnotification.NotificationId = int.Parse(dr["NotificationId"].ToString());
                    objnotification.userid = int.Parse(dr["userid"].ToString());
                    objnotification.device_id = dr["device_id"].ToString();
                    objnotification.device_type = dr["device_type"].ToString();
                    objnotification.subject = dr["subject"].ToString();
                    objnotification.shortdescription = dr["shortdescription"].ToString();
                    objnotification.description = dr["description"].ToString();

                    DateTime newcreatedon = Convert.ToDateTime(dr["createdOn"].ToString());
                    objnotification.CreatedDate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", newcreatedon);

                    ////DateTime newcreatedon = Convert.ToDateTime(dr["createdOn"].ToString());
                    //objnotification.CreatedDate = dr["createdOn"].ToString();


                    objnotificationlist.Add(objnotification);

                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, objnotificationlist);
            return response;

        }


    }
}
