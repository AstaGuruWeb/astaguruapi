﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Astaguru_webapi.Controllers
{
    [EnableCors(origins: "http://localhost:56311/", headers: "*", methods: "*")]
    public class SaveDeviceController : ApiController
    {
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        Utility util = new Utility();
        HttpResponseMessage response;


        public HttpResponseMessage Post(Pushnotification obj)
        {
            Pushnotification responsPushnotification = new Pushnotification();
            try
            {
                DataTable dt = new DataTable();

                dt = util.Display("Exec Proc_Savepushdevice 'GetUserByuserId'," + obj.userid + "");

                if (dt.Rows.Count > 0)
                {
                    int dtuserid = int.Parse(dt.Rows[0]["userid"].ToString());
                    if (dtuserid == obj.userid)
                    {
                        using (SqlCommand cmd = new SqlCommand("Proc_Savepushdevice"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Para", "Update");
                            cmd.Parameters.AddWithValue("@userid", obj.userid);
                            cmd.Parameters.AddWithValue("@device_id", obj.device_id);
                            cmd.Parameters.AddWithValue("@device_type", obj.device_type);
                            if (util.Execute(cmd))
                            {
                                responsPushnotification.userid = obj.userid;
                                responsPushnotification.device_id = obj.device_id;
                                responsPushnotification.device_type = obj.device_type;
                                response = Request.CreateResponse(HttpStatusCode.OK, responsPushnotification);
                                return response;
                            }


                        }
                    }
                }
                else
                {
                    using (SqlCommand cmd = new SqlCommand("Proc_Savepushdevice"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Para", "Add");
                        cmd.Parameters.AddWithValue("@userid", obj.userid);
                        cmd.Parameters.AddWithValue("@device_id", obj.device_id);
                        cmd.Parameters.AddWithValue("@device_type", obj.device_type);
                        if (util.Execute(cmd))
                        {
                            responsPushnotification.userid = obj.userid;
                            responsPushnotification.device_id = obj.device_id;
                            responsPushnotification.device_type = obj.device_type;
                            response = Request.CreateResponse(HttpStatusCode.OK, responsPushnotification);
                            return response;
                        }

                    }
                }
                return response;

            }
            catch (Exception ex)
            {
                responsPushnotification.userid = 0;
                responsPushnotification.device_id = "";
                responsPushnotification.device_type = "";
                response = Request.CreateResponse(HttpStatusCode.BadRequest, responsPushnotification);
                return response;
            }
        }
    }
}
