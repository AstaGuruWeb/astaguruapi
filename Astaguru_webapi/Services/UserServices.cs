﻿using Astaguru_webapi.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Astaguru_webapi.Services
{
    public class UserServices
    {
        Log log = new Log();
        public User GetUserId(string emailId, string password)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUserId");
                    para.Add("@email", emailId);
                    para.Add("@password", password);
                    var value = con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    return value;
                }
           

                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public User GetBillingAddress(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetBillingAddress");
                    para.Add("@userid", userid);
                    var value = con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    return value;
                }
             

                     catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public List<User> GetCountryCode()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCountryCode");
                    return con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
        

                      catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public string CheckMobileNumber(string Mobile)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "CheckMobileNumber");
                    para.Add("@Mobile", Mobile); // Normal Parameters  
                    return con.Query<string>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }

                       catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }
        public List<User> GetAboutUs()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAboutUs");
                    return con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }

                       catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public List<User> GetinterestMaster()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetinterestMaster");
                    return con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }


                         catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public int InsertRegistration(User UL)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "InsertRegistration");
                    para.Add("@email", UL.email);
                    para.Add("@password", UL.password);
                    para.Add("@Mobile", UL.Mobile);
                    para.Add("@name", UL.name);
                    para.Add("@address1", UL.address1);
                    para.Add("@address2", UL.address2);
                    para.Add("@city", UL.city);
                    para.Add("@state", UL.state);
                    para.Add("@country", UL.country);
                    para.Add("@zip", UL.zip);
                    para.Add("@lastname", UL.lastname);
                    para.Add("@activationcode", UL.activationcode);
                    para.Add("@SmsCode", UL.SmsCode);
                    para.Add("@MobileVerified", UL.MobileVerified);
                    para.Add("@genderid", UL.genderid);
                    para.Add("@bday", UL.bday);
                    para.Add("@bmonth", UL.bmonth);
                    para.Add("@byear", UL.byear);
                    para.Add("@aboutId", UL.aboutId);
                    para.Add("@interestedIds", UL.interestedIds);
                    para.Add("@countryCode", UL.countryCode);
                    para.Add("@username", UL.username);
                    para.Add("@activationcode", UL.activationcode);
                    para.Add("@nickname", UL.nickname);
                    para.Add("@countryid", UL.countryid);
                    para.Add("@stateid", UL.stateid);
                    para.Add("@cityid", UL.cityid);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
           

                         catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public int UpdateRegistration(User UL)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateRegistration");
                    para.Add("@MobileVerified", UL.MobileVerified);


                    para.Add("@companyName", UL.companyName);
                    para.Add("@BillingCountry", UL.BillingCountry);
                    para.Add("@BillingAddress", UL.BillingAddress);
                    para.Add("@billingAddress2", UL.billingAddress2);
                    para.Add("@BillingState", UL.BillingState);
                    para.Add("@BillingZip", UL.BillingZip);
                    para.Add("@BillingCity", UL.BillingCity);
                    para.Add("@nickname", UL.nickname);

                    para.Add("@GSTIN", UL.GSTIN);
                    para.Add("@panCard", UL.panCard);
                    para.Add("@acNumber", UL.acNumber);
                    para.Add("@holderName", UL.holderName);
                    para.Add("@ifscCode", UL.ifscCode);
                    para.Add("@branchName", UL.branchName);
                    para.Add("@swiftCode", UL.swiftCode);
                    para.Add("@imagePanCard", UL.imagePanCard);
                    para.Add("@imageAadharCard", UL.imageAadharCard);

                    para.Add("@genderid", UL.genderid);
                    para.Add("@bday", UL.bday);
                    para.Add("@bmonth", UL.bmonth);
                    para.Add("@byear", UL.byear);
                    para.Add("@interestedIds", UL.interestedIds);

                    para.Add("@address1", UL.address1);
                    para.Add("@address2", UL.address2);
                    para.Add("@city", UL.city);
                    para.Add("@state", UL.state);
                    para.Add("@country", UL.country);
                    para.Add("@zip", UL.zip);
                    para.Add("@aadharCard", UL.aadharCard);

                    para.Add("@countryid", UL.countryid);
                    para.Add("@stateid", UL.stateid);
                    para.Add("@cityid", UL.cityid);

                    para.Add("@bCountryid", UL.bCountryid);
                    para.Add("@bStateid", UL.bStateid);
                    para.Add("@bCityid", UL.bCityid);

                    para.Add("@userid", UL.userid);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
              catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public int UpdatePassword(User UL)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdatePassword");
                    para.Add("@password", UL.password);
                    para.Add("@userid", UL.userid);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
     
                 catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public int GetCheckEmail(User UL)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCheckEmail");
                    para.Add("@email", UL.email);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }


                 catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public string GetCheckUsername(User UL)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCheckUsername");
                    para.Add("@username", UL.username);
                    return con.Query<string>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
             
                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public User GetProfileData(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProfileData");
                    para.Add("@userid", userid);
                    var value = con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    return value;
                }
                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public User GetEmailVerified(string activationcode)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetEmailVerified");
                    para.Add("@activationcode", activationcode);
                    return con.Query<User>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }

                     catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public int UpdateEmailVerified(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateEmailVerified");
                    para.Add("@userid", userid);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }

                       catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public int UpdateMobileVerified(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateMobileVerified");
                    para.Add("@userid", userid);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
                         catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public int UpdateBidLimit(int userid, int amountlimt)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateBidLimit");
                    para.Add("@userid", userid);
                    para.Add("@amountlimt", amountlimt);
                    return con.Query<int>("CRUDUSER", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// created on 12_08_2020

        public void sendoutbidmsg(string reference, string username, string mobile)
        {
            string authKey = "105048AuvvUDCr56c199f0";
            //Multiple mobiles numbers separated by comma
            string mobileNumber = mobile;
            //Sender ID,While using route4 sender id should be 6 characters long.
            string senderId = "ASTGUR";
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode("Dear " + username + ", please note you have been outbid on Lot No." + reference.Trim() + ". Place renew bid on www.astaguru.com or mobile App");



            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authKey);
            sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
            sbPostData.AppendFormat("&message={0}", message);
            sbPostData.AppendFormat("&sender={0}", senderId);
            sbPostData.AppendFormat("&route={0}", "4");

            try
            {
                //Call Send SMS API
                string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();
            }
            catch (SystemException ex)
            {

            }
        }

    }
}