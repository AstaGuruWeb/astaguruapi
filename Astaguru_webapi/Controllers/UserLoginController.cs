﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class UserLoginController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;
        public HttpResponseMessage Post(User obj)
        {
            List<User> obuserList = new List<User>();
            User objuser = new User();

            DataTable dt = new DataTable();
            dt = util.Display("Exec spUserLogin '"+ obj.username + "','"+ obj.password + "','"+ obj.deviceTocken + "','"+ obj.deviceType + "'");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    
                    objuser.userid = int.Parse(dr["userid"].ToString());
                    objuser.username = dr["username"].ToString();
                    objuser.password = dr["password"].ToString();
                    objuser.name = dr["name"].ToString();
                    objuser.address1 = dr["address1"].ToString();
                    objuser.address2 = dr["address2"].ToString();
                    objuser.city = dr["city"].ToString();
                    objuser.state = dr["state"].ToString();
                    objuser.country = dr["country"].ToString();
                    objuser.zip = dr["zip"].ToString();
                    objuser.telephone = dr["telephone"].ToString();
                    objuser.fax = dr["fax"].ToString();
                    objuser.email = dr["email"].ToString();
                    objuser.admin = dr["admin"].ToString();
                    objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                    objuser.LastLoggedDate = dr["LastLoggedDate"].ToString();

                    if (dr["Visits"].ToString() == "")
                    {
                        objuser.Visits = 0;
                    }
                    else
                    {
                        objuser.Visits = int.Parse(dr["Visits"].ToString());
                    }


                    if (dr["buy"].ToString() == "")
                    {
                        objuser.buy = 0;
                    }
                    else
                    {
                        objuser.buy = int.Parse(dr["buy"].ToString());
                    }

                    objuser.nickname = dr["nickname"].ToString();

                    if (dr["confirmbid"].ToString() == "")
                    {
                        objuser.confirmbid = 0;
                    }
                    else
                    {
                        objuser.confirmbid = int.Parse(dr["confirmbid"].ToString());
                    }

                    if (dr["anonumber"].ToString() == "")
                    {
                        objuser.anonumber = 0;
                    }
                    else
                    {
                        objuser.anonumber = int.Parse(dr["anonumber"].ToString());
                    }

                    if (dr["amountlimt"].ToString() == "")
                    {
                        objuser.amountlimt = 0;
                    }
                    else
                    {
                        objuser.amountlimt = int.Parse(dr["amountlimt"].ToString());
                    }

                    objuser.lastname = dr["lastname"].ToString();
                    objuser.applyforbid = dr["applyforbid"].ToString();
                    objuser.Subscribe = dr["Subscribe"].ToString();
                    objuser.blocked = dr["blocked"].ToString();
                    objuser.BillingName = dr["BillingName"].ToString();
                    objuser.BillingAddress = dr["BillingAddress"].ToString();
                    objuser.BillingCity = dr["BillingCity"].ToString();
                    objuser.BillingState = dr["BillingState"].ToString();
                    objuser.BillingCountry = dr["BillingCountry"].ToString();
                    objuser.BillingZip = dr["BillingZip"].ToString();
                    objuser.BillingTelephone = dr["BillingTelephone"].ToString();
                    objuser.BillingEmail = dr["BillingEmail"].ToString();
                    objuser.Mobile = dr["Mobile"].ToString();
                    objuser.activationcode = dr["activationcode"].ToString();
                    objuser.ip = dr["ip"].ToString();
                    objuser.IPCountry = dr["IPCountry"].ToString();
                    objuser.SmsCode = dr["SmsCode"].ToString();
                    objuser.MobileVerified = dr["MobileVerified"].ToString();
                    objuser.t_username = dr["t_username"].ToString();
                    objuser.t_password = dr["t_password"].ToString();
                    objuser.t_firstname = dr["t_firstname"].ToString();
                    objuser.t_address1 = dr["t_address1"].ToString();
                    objuser.t_address2 = dr["t_address2"].ToString();
                    objuser.t_City = dr["t_City"].ToString();
                    objuser.t_State = dr["t_State"].ToString();
                    objuser.t_Country = dr["t_Country"].ToString();
                    objuser.t_zip = dr["t_zip"].ToString();
                    objuser.t_telephone = dr["t_telephone"].ToString();
                    objuser.t_fax = dr["t_fax"].ToString();
                    objuser.t_email = dr["t_email"].ToString();
                    objuser.t_billingname = dr["t_billingname"].ToString();
                    objuser.t_billingaddress = dr["t_billingaddress"].ToString();
                    objuser.t_billingcity = dr["t_billingcity"].ToString();
                    objuser.t_billingstate = dr["t_billingstate"].ToString();
                    objuser.t_billingcountry = dr["t_billingcountry"].ToString();
                    objuser.t_billingzip = dr["t_billingzip"].ToString();
                    objuser.t_lastname = dr["t_lastname"].ToString();
                    objuser.t_nickname = dr["t_nickname"].ToString();
                    objuser.t_mobile = dr["t_mobile"].ToString();
                    objuser.t_billingemail = dr["t_billingemail"].ToString();
                    objuser.t_billingtelephone = dr["t_billingtelephone"].ToString();
                    objuser.applyforchange = dr["applyforchange"].ToString();
                    objuser.EmailVerified = dr["EmailVerified"].ToString();
                    objuser.otptime = dr["otptime"].ToString();
                    objuser.chatdept = dr["chatdept"].ToString();
                    objuser.deviceTocken = dr["deviceTocken"].ToString();
                    objuser.androidDeviceTocken = dr["androidDeviceTocken"].ToString();
                    if (dr["genderid"].ToString() == "")
                    {
                        objuser.genderid = 0;
                    }
                    else
                    {
                        objuser.genderid = int.Parse(dr["genderid"].ToString());
                    }

                    if (dr["bday"].ToString() == "")
                    {
                        objuser.bday = 0;
                    }
                    else
                    {
                        objuser.bday = int.Parse(dr["bday"].ToString());
                    }


                    if (dr["bmonth"].ToString() == "")
                    {
                        objuser.bmonth = 0;
                    }
                    else
                    {
                        objuser.bmonth = int.Parse(dr["bmonth"].ToString());
                    }

                    if (dr["byear"].ToString() == "")
                    {
                        objuser.byear = 0;
                    }
                    else
                    {
                        objuser.byear = int.Parse(dr["byear"].ToString());
                    }

                    objuser.aboutId = dr["aboutId"].ToString();
                    objuser.interestedIds = dr["interestedIds"].ToString();
                    objuser.countryCode = dr["countryCode"].ToString();
                    objuser.GSTIN = dr["GSTIN"].ToString();
                    objuser.panCard = dr["panCard"].ToString();
                    objuser.acNumber = dr["acNumber"].ToString();
                    objuser.holderName = dr["holderName"].ToString();
                    objuser.ifscCode = dr["ifscCode"].ToString();
                    objuser.branchName = dr["branchName"].ToString();
                    objuser.swiftCode = dr["swiftCode"].ToString();
                    objuser.imagePanCard = dr["imagePanCard"].ToString();
                    objuser.companyName = dr["companyName"].ToString();
                    objuser.aadharCard = dr["aadharCard"].ToString();
                    objuser.billingAddress2 = dr["billingAddress2"].ToString();
                    objuser.imageAadharCard = dr["imageAadharCard"].ToString();
                    if (dr["countryid"].ToString() == "")
                    {
                        objuser.countryid = 0;
                    }
                    else
                    {
                        objuser.countryid = int.Parse(dr["countryid"].ToString());
                    }

                    if (dr["stateid"].ToString() == "")
                    {
                        objuser.stateid = 0;
                    }
                    else
                    {
                        objuser.stateid = int.Parse(dr["stateid"].ToString());
                    }


                    if (dr["cityid"].ToString() == "")
                    {
                        objuser.stateid = 0;
                    }
                    else
                    {
                        objuser.cityid = int.Parse(dr["stateid"].ToString());
                    }

                    if (dr["bCountryid"].ToString() == "")
                    {
                        objuser.bCountryid = 0;
                    }
                    else
                    {
                        objuser.bCountryid = int.Parse(dr["bCountryid"].ToString());
                    }

                    if (dr["bStateid"].ToString() == "")
                    {
                        objuser.bStateid = 0;
                    }
                    else
                    {
                        objuser.bStateid = int.Parse(dr["bStateid"].ToString());
                    }


                    if (dr["bCityid"].ToString() == "")
                    {
                        objuser.bCityid = 0;
                    }
                    else
                    {
                        objuser.bCityid = int.Parse(dr["bCityid"].ToString());
                    }

                    if (dr["isOldUser"].ToString() == "")
                    {
                        objuser.isOldUser = 0;
                    }
                    else
                    {
                        objuser.isOldUser = int.Parse(dr["isOldUser"].ToString());
                    }





                    obuserList.Add(objuser);

                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, obuserList);
            return response;

        }
    }
}
