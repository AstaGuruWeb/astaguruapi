﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class AddToGallary
    {
        public int productID { get; set; }

        public int UserId { get; set; }
    }

    public class addtogallaryResponse
    {
        public string Message { get; set; }
    }
}