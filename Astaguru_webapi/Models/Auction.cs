﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Auction
    {
        public int productid { get; set; }

        public int id { get; set; }


        public string title { get; set; }
        public string name { get; set; }
        public string auctiontitle { get; set; }


        public string firstname { get; set; }

        public string lastname { get; set; }

        public string thumbnail { get; set; }

        public string image { get; set; }


        public string Date { get; set; }

        public int mediumid { get; set; }
        public string medium { get; set; }

        public string productsize { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime Bidclosingtime { get; set; }

        public int price { get; set; }

        [Column("Bidpricers")]

        public int pricers { get; set; }
        public int priceus { get; set; }

        public int curprice { get; set; }

        public int curpriceUs { get; set; }



        public string collectors { get; set; }

        public string estamiate { get; set; }

        public int userid { get; set; }

        public string username { get; set; }

        public int Auctionid { get; set; }
        public string reference { get; set; }

        public string anoname { get; set; }


        public int ProxyAmt { get; set; }

        public int ProxyAmtus { get; set; }

        public int recentbid { get; set; }
        public int currentbid { get; set; }


        // pricelow
        public int nextValidBidRs { get; set; }
        public int nextValidBidUs { get; set; }


        public string nickname { get; set; }

        public int proxy { get; set; }

        public string proxy1 { get; set; }

        public int Bidrecordid { get; set; }

        public int validbidpricers { get; set; }
        public int validbidpriceus { get; set; }

        public int Bidpricers { get; set; }
        public int Bidpriceus { get; set; }

        public string Profile { get; set; }

        public string Auctionname { get; set; }

        public string auctiondate { get; set; }

        public string daterec { get; set; }


        public int totalrs { get; set; }
        public int totalus { get; set; }


        public int pricelow { get; set; }
        public int pricehigh { get; set; }

        public string description { get; set; }

        public int auctionType { get; set; }

        public int artistid { get; set; }

        public string Picture { get; set; }

        public int categoryid { get; set; }

        public string category { get; set; }
        public string HumanFigure { get; set; }


        // Filter

        public List<string> chkArtist { get; set; }

        public List<string> chkMedium { get; set; }

        public List<string> chkDept { get; set; }

        public string view1 { get; set; }
        public string view2 { get; set; }
        public string view3 { get; set; }
        public string view4 { get; set; }

        public int VAT { get; set; }

        public string ProductImage { get; set; }


        public int currency { get; set; }

        public string auctionBanner { get; set; }
        public string Prdescription { get; set; }

        public string myBidClosingTime { get; set; }


        public string hh { get; set; }
        public string mm { get; set; }
        public string ss { get; set; }


        public int timeRemains { get; set; }

        public int MyUserID { get; set; }


        public string Subquery { get; set; }

        public int page { get; set; }

        public string pageName { get; set; } // For get the current page name

        public int Online { get; set; }


        public string ipAddress { get; set; }
        public string userLocation { get; set; }
        public string browserName { get; set; }
        public string fullAddress { get; set; }

        public string shortAddress { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }

        public string email { get; set; }

        public int LastBidId { get; set; }

        public int mailPreprice { get; set; }
        public int mailPrepriceUs { get; set; }

        public string status { get; set; }
        public string productdate { get; set; }

        public int amountlimt { get; set; }

        public string country { get; set; }

        public List<Auction> GetReviewData;


        public string reviewStatus { get; set; }

        public int isOldUser { get; set; }

        public string PrVat { get; set; }

        public int isInternational { get; set; }

        public int isInternationalGST { get; set; }

        public int astaguruPrice { get; set; }
        public string auctionWebImage { get; set; }

        public string listImage { get; set; }

        public string recentAuctionBanner { get; set; }

        public int usedGoodPercentage { get; set; }

        //created on 29_01_20
        public int checkbidlimit { get; set; }

        public int islessproxy { get; set; }

        public string bidByVal { get; set; }
        public string deviceTocken { get; set; }
        public string OSversion { get; set; }
        public string modelName { get; set; }


        public int checknextvalidbid { get; set; }


        public string Ownerid { get; set; }

        public string mobile { get; set; }

        //added on 27_07_2021
        public int confirmbid { get; set; }
    }
}