﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Astaguru_webapi.Controllers
{
    //[EnableCors(origins: "http://localhost:56311/", headers: "*", methods: "*")]
    public class GetHomeBannerController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;

        public HttpResponseMessage Get()
        {
            List<HomeBanner> objaucList = new List<HomeBanner>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec spGetHomeBanner");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    HomeBanner objauc = new HomeBanner();
                    objauc.homebannerID = int.Parse(dr["homebannerID"].ToString());
                    objauc.homebannerTitle = dr["homebannerTitle"].ToString();
                    objauc.homebannerImg = dr["homebannerImg"].ToString();
                    objauc.type = int.Parse(dr["type"].ToString());
                    objauc.bannerType = int.Parse(dr["bannerType"].ToString());
                    objauc.dateAdded = dr["dateAdded"].ToString();
                    objauc.auctionId = int.Parse(dr["auctionId"].ToString());
                    objauc.auctionPageUrl = dr["auctionPageUrl"].ToString();
                    objauc.urlID = dr["urlID"].ToString();
                    objauc.Auctionname = dr["Auctionname"].ToString();
                    objauc.versionAndroid = dr["versionAndroid"].ToString();
                    objauc.versionIOS = dr["versionIOS"].ToString();
                    objauc.webImages = dr["webImages"].ToString();
                    objauc.livecount = dr["livecount"].ToString();

                    objaucList.Add(objauc);

                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, objaucList);
            return response;

        }

    }
}
