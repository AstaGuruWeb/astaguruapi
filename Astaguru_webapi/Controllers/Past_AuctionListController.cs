﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class Past_AuctionListController : ApiController
    {
        Utility util = new Utility();
        
        public resourceAuctionlist Get(string auctionstatus)
        {
            List<AuctionList> objaucList = new List<AuctionList>();

            DataTable dt = new DataTable();
            dt = util.Display("select CAST(DollarRate As int)as DollarRate1,Auctionid,Auctionname,Date,image,auctiondate,auctiontitle,bidpdf,status,totalSaleValueRs,totalSaleValueUs,auctionBanner,upcomingCountVal from getPastAuctionList where status='" + auctionstatus + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    AuctionList objAuction = new AuctionList();
                    objAuction.AuctionId = int.Parse(dr["Auctionid"].ToString());
                    objAuction.Auctionname = dr["Auctionname"].ToString();
                    objAuction.Date = dr["Date"].ToString();
                    objAuction.DollarRate = int.Parse(dr["DollarRate1"].ToString());

                    objAuction.image = dr["image"].ToString();
                    objAuction.auctiondate = dr["auctiondate"].ToString();
                    objAuction.auctiontitle = dr["auctiontitle"].ToString();
                    objAuction.image = dr["image"].ToString();
                    objAuction.bidpdf = dr["bidpdf"].ToString();
                    objAuction.status = dr["status"].ToString();
                    objAuction.totalSaleValueRs = dr["totalSaleValueRs"].ToString();
                    objAuction.totalSaleValueUs = dr["totalSaleValueUs"].ToString();

                    objAuction.auctionBanner = dr["auctionBanner"].ToString();

                    objAuction.upcomingCountVal = int.Parse(dr["upcomingCountVal"].ToString());
                    //objAuction.auctionType = int.Parse(dr["auctionType"].ToString());


                    objaucList.Add(objAuction);

                }
            }
            resourceAuctionlist objresource = new resourceAuctionlist();
            objresource.resource = objaucList;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
