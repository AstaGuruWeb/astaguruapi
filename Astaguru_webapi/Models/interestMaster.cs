﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class interestMaster
    {
        public int interestedId { get; set; }

        public string interest { get; set; }

        public int status { get; set; }
    }

    public class resourceinterestMaster
    {
        public List<interestMaster> resource { get; set; }
    }
}