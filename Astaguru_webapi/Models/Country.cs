﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Country
    {
        public int countryid { get; set; }

        public string countryname { get; set; }

        public string countrycode { get; set; }
    }

  

    public class resourceCountry
    {
        public List<Country> resource { get; set; }
    }


    public class state
    {

        public int stateid { get; set; }

        public string statename { get; set; }
        public int countryid { get; set; }

       
    }

    public class resourcestate
    {
        public List<state> resource { get; set; }
    }


    public class City
    {
        public int cityid { get; set; }
        public string cityname { get; set; }
        public int stateid { get; set; }

    }

    public class resourceCity
    {
        public List<City> resource { get; set; }
    }
}