﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class FilterUserController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;


        public resourceUsers Get(string userid)
        {
            List<User> objaucList = new List<User>();

            DataTable dt = new DataTable();
            //dt = util.Display("select * from users where userid='" + userid + "' or username='" + username + "'or email='" + email + "'");
            dt = util.Display("select * from users where userid='" + userid + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    User objuser = new User();

                    objuser.userid = int.Parse(dr["userid"].ToString());
                    objuser.name = dr["name"].ToString();
                    objuser.Mobile = dr["Mobile"].ToString();
                    objuser.email = dr["email"].ToString();
                    objuser.lastname = dr["lastname"].ToString();
                    objuser.address1 = dr["address1"].ToString();
                    objuser.city = dr["city"].ToString();
                    objuser.zip = dr["zip"].ToString();
                    objuser.state = dr["state"].ToString();
                    objuser.country = dr["country"].ToString();
                    objuser.telephone = dr["telephone"].ToString();
                    objuser.password = dr["password"].ToString();
                    objuser.fax = dr["fax"].ToString();
                    objuser.BillingName = dr["BillingName"].ToString();
                    objuser.BillingAddress = dr["BillingAddress"].ToString();
                    objuser.BillingCity = dr["BillingCity"].ToString();
                    objuser.BillingState = dr["BillingState"].ToString();
                    objuser.BillingCountry = dr["BillingCountry"].ToString();
                    objuser.BillingZip = dr["BillingZip"].ToString();
                    objuser.BillingTelephone = dr["BillingTelephone"].ToString();
                    objuser.BillingEmail = dr["BillingEmail"].ToString();
                    if (dr["bCountryid"].ToString() == "")
                    {
                        objuser.bCountryid = 0;
                    }
                    else
                    {
                        objuser.bCountryid = int.Parse(dr["bCountryid"].ToString());
                    }

                    if (dr["bCityid"].ToString() == "")
                    {
                        objuser.bCityid = 0;
                    }
                    else
                    {
                        objuser.bCityid = int.Parse(dr["bCityid"].ToString());
                    }

                 
                    objuser.t_username = dr["t_username"].ToString();
                    objuser.androidDeviceTocken = dr["androidDeviceTocken"].ToString();
                    objuser.t_fax = dr["t_fax"].ToString();
                    objuser.t_billingname = dr["t_billingname"].ToString();
                    objuser.t_mobile = dr["t_mobile"].ToString();
                    objuser.t_address1 = dr["t_address1"].ToString();
                    objuser.t_billingemail = dr["t_billingemail"].ToString();
                    objuser.applyforbid = dr["applyforbid"].ToString();
                    objuser.t_billingaddress = dr["t_billingaddress"].ToString();
                    if (dr["genderid"].ToString() == "")
                    {
                        objuser.genderid = 0;
                    }
                    else
                    {
                        objuser.genderid = int.Parse(dr["genderid"].ToString());
                    }

                    if (dr["cityid"].ToString() == "")
                    {
                        objuser.cityid = 0;
                    }
                    else
                    {
                        objuser.cityid = int.Parse(dr["cityid"].ToString());
                    }

               
                    objuser.chatdept = dr["chatdept"].ToString();

                    if (dr["bmonth"].ToString() == "")
                    {
                        objuser.bmonth = 0;
                    }
                    else
                    {
                        objuser.bmonth = int.Parse(dr["bmonth"].ToString());
                    }
                ;
                    objuser.applyforchange = dr["applyforchange"].ToString();
                    objuser.MobileVerified = dr["MobileVerified"].ToString();
                    objuser.t_firstname = dr["t_firstname"].ToString();
                    objuser.t_billingcity = dr["t_billingcity"].ToString();
                    objuser.t_billingcountry = dr["t_billingcountry"].ToString();
                    objuser.t_email = dr["t_email"].ToString();
                    objuser.t_State = dr["t_State"].ToString();

                    if (dr["bStateid"].ToString() == "")
                    {
                        objuser.bStateid = 0;
                    }
                    else
                    {
                        objuser.bStateid = int.Parse(dr["bStateid"].ToString());
                    }
                
                    objuser.t_telephone = dr["t_telephone"].ToString();
                    objuser.t_billingzip = dr["t_billingzip"].ToString();
                    objuser.EmailVerified = dr["EmailVerified"].ToString();
                    objuser.address2 = dr["address2"].ToString();
                    objuser.nickname = dr["nickname"].ToString();
                    objuser.admin = dr["admin"].ToString();
                    objuser.t_City = dr["t_City"].ToString();
                    if (dr["byear"].ToString() == "")
                    {
                        objuser.byear = 0;
                    }
                    else
                    {
                        objuser.byear = int.Parse(dr["byear"].ToString());
                    }
                  
                    objuser.t_lastname = dr["t_lastname"].ToString();

                    if (dr["Visits"].ToString() == "")
                    {
                        objuser.Visits = 0;
                    }
                    else
                    {
                        objuser.Visits = int.Parse(dr["Visits"].ToString());
                    }
                   
                    objuser.t_password = dr["t_password"].ToString();
                    if (dr["countryid"].ToString() == "")
                    {
                        objuser.countryid = 0;
                    }
                    else
                    {
                        objuser.countryid = int.Parse(dr["countryid"].ToString());
                    }
             
                    objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                    objuser.t_Country = dr["t_Country"].ToString();
                    if (dr["confirmbid"].ToString() == "")
                    {
                        objuser.confirmbid = 0;
                    }
                    else
                    {
                        objuser.confirmbid = int.Parse(dr["confirmbid"].ToString());
                    }
                 
                    objuser.aboutId = dr["aboutId"].ToString();
                    objuser.t_billingtelephone = dr["t_billingtelephone"].ToString();
                    if (dr["bday"].ToString() == "")
                    {
                        objuser.bday = 0;
                    }
                    else
                    {
                        objuser.bday = int.Parse(dr["bday"].ToString());
                    }
                   
                    objuser.SmsCode = dr["SmsCode"].ToString();
                    if (dr["buy"].ToString() == "")
                    {
                        objuser.buy = 0;
                    }
                    else
                    {
                        objuser.buy = int.Parse(dr["buy"].ToString());
                    }
              
                    objuser.username = dr["username"].ToString();
                    objuser.t_nickname = dr["t_nickname"].ToString();
                    objuser.t_billingstate = dr["t_billingstate"].ToString();
                    objuser.deviceTocken = dr["deviceTocken"].ToString();
                    objuser.interestedIds = dr["interestedIds"].ToString();

                    objuser.GSTIN = dr["GSTIN"].ToString();
                    objuser.panCard = dr["panCard"].ToString();
                    objuser.acNumber = dr["acNumber"].ToString();
                    objuser.holderName = dr["holderName"].ToString();
                    objuser.ifscCode = dr["ifscCode"].ToString();
                    objuser.branchName = dr["branchName"].ToString();
                    objuser.swiftCode = dr["swiftCode"].ToString();
                    objuser.imagePanCard = dr["imagePanCard"].ToString();
                    objuser.imageAadharCard = dr["imageAadharCard"].ToString();
                    objuser.companyName = dr["companyName"].ToString();
                    objuser.aadharCard = dr["aadharCard"].ToString();

                    if (dr["amountlimt"].ToString() == "")
                    {
                        objuser.amountlimt = 0;
                    }
                    else
                    {
                        objuser.amountlimt = int.Parse(dr["amountlimt"].ToString());
                    }

                    objuser.isOldUser= int.Parse(dr["isOldUser"].ToString());

                    objaucList.Add(objuser);

                }
            }

            resourceUsers objresource = new resourceUsers();
            objresource.resource = objaucList;
            response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);

            return objresource;

        }
    }
}
