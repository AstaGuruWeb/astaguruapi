﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class CRMDataAddController : ApiController
    {
        HttpResponseMessage response;
        Utility util = new Utility();

        public HttpResponseMessage Post(Categories objuser)
        {
            try
            {
                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand("spAddCategory"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                
                    cmd.Parameters.AddWithValue("@pinCategory", objuser.category);
               


                    if (util.Execute(cmd))
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, response);

                    }
                 
                }

                return response;
            }
            catch (Exception Ex)
            {

                response = Request.CreateResponse(HttpStatusCode.BadRequest, response);
                return response;
            }
        }
    }
}

