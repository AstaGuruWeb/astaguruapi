﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Upcomming_Proxy
    {
        public string currentStatus { get; set; }
        public string msg { get; set; }
        public string auctionDate { get; set; }
    }
}