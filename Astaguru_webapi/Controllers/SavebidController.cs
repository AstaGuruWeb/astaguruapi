﻿using Astaguru_webapi.Models;
using Astaguru_webapi.Services;
using Astaguru_webapi.Services.UserService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Astaguru_webapi.Controllers
{
    //[EnableCors(origins: "http://localhost:56311/", headers: "*", methods: "*")]
    [EnableCors(origins: "http://asta.kwebmakerdigital.com/", headers: "*", methods: "*")]
    public class SavebidController : ApiController
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        CurrentAuctionService CAS = new CurrentAuctionService();
        CommonController COMC = new CommonController();
        UserServices US = new UserServices();
        Auction auc = new Auction();
        List<Auction> listAuction = new List<Auction>();




        HttpResponseMessage response;

        public HttpResponseMessage Post(Auction AUC)
        {
            Responsemessage responsemessage = new Responsemessage();
            int browsercurrentbid = AUC.checknextvalidbid;   ///get current bid value of product

            try
            {
               

                //int isOldUser = AUC.isOldUser;
                //int amountlimt = AUC.amountlimt;
                string country = AUC.country;
                string username = AUC.username;
                string nickname = AUC.nickname;

                string bidByVal = AUC.bidByVal;
                string deviceTocken = AUC.deviceTocken;
                string OSversion = AUC.OSversion;
                string modelName = AUC.modelName;
                string ipAddress = AUC.ipAddress;
                string latitude = AUC.latitude;
                string longitude = AUC.longitude;
                string fullAddress = AUC.fullAddress;
                string userLocation = AUC.userLocation;


                int userid = AUC.userid;   //COMC.checkUserSessionValue();
                int amountlimt = CAS.Getuserlimit(userid);

                if (userid > 0)
                {
                    int isPresentArtist = CAS.CheckArtistAssginedbidUserlist(AUC);
                    if (isPresentArtist > 0)
                    {

                    }
                    else
                    {
                        int addArtist = CAS.AddArtisttoBidUserList(AUC);
                    }

                    AUC = CAS.GetCurrentAuctionDetail(AUC.productid);


                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.pricers, 1).ToString());
                    AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;

                    //for response
                    #region
                    int BidAmountRs = AUC.nextValidBidRs;
                    int BidAmountUs = AUC.nextValidBidUs;

                    User UR = US.GetBillingAddress(userid);
                    #endregion

                    //added on 27_07_2021
                    //Get ConfirmedBid value during Bidding

                    //auc.confirmbid = CAS.getConfirmedBidValue(userid);

                    Auction objauction = CAS.getConfirmedBidIsOlduserValue(userid);
                    int isOldUser = objauction.isOldUser;

                    if (objauction.confirmbid == 0)
                    {
                        responsemessage.currentStatus = 9;
                        responsemessage.msg = "You do not have Bidding Access. Please contact Astaguru.";
                        responsemessage.mobileNum = UR.Mobile;
                        responsemessage.emailID = UR.email;
                        responsemessage.Username = AUC.username;
                        responsemessage.BidAmountRs = BidAmountRs;
                        responsemessage.BidAmountUs = BidAmountUs;
                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                        return response;
                    }


                    // is user bid limit Amount Exceed or not

                    if (isOldUser == 0)
                    {
                        if (country == "India")
                        {
                            if (AUC.nextValidBidRs > amountlimt)
                            {

                                responsemessage.currentStatus = 1;
                                responsemessage.msg = "Your Bid Limit Exceeded. Kindly Contact with Your Administrator";
                                responsemessage.mobileNum = UR.Mobile;
                                responsemessage.emailID = UR.email;
                                responsemessage.Username = AUC.username;
                                responsemessage.BidAmountRs = BidAmountRs;
                                responsemessage.BidAmountUs = BidAmountUs;

                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                                return response;
                            }
                        }
                        else
                        {
                            if (AUC.nextValidBidUs > amountlimt)
                            {

                                responsemessage.currentStatus = 1;
                                responsemessage.msg = "Your Bid Limit Exceeded. Kindly Contact with Your Administrator";
                                responsemessage.mobileNum = UR.Mobile;
                                responsemessage.emailID = UR.email; ;
                                responsemessage.Username = AUC.username;
                                responsemessage.BidAmountRs = BidAmountRs;
                                responsemessage.BidAmountUs = BidAmountUs;
                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);                       
                                return response;
                            }
                        }
                    }

                    var result = AUC.Ownerid.Split(',');
                    for (int i = 0; i < result.Length; i++)
                    {
                        int ownerid = int.Parse(result[i]);
                        if (ownerid == userid)
                        {
                            responsemessage.currentStatus = 7;
                            responsemessage.msg = "Sorry, you're not permitted to bid on this lot. Please contact our office for support.";
                            responsemessage.mobileNum = UR.Mobile;
                            responsemessage.emailID = UR.email; 
                            responsemessage.Username = AUC.username;
                            responsemessage.BidAmountRs = BidAmountRs;
                            responsemessage.BidAmountUs = BidAmountUs;
                            response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                            return response;
                        }
                    }

                    //if (AUC.Ownerid == userid)
                    //{
                    //    responsemessage.currentStatus = 7;
                    //    responsemessage.msg = "Sorry, you're not permitted to bid on this lot. Please contact our office for support.";
                    //    responsemessage.mobileNum = UR.Mobile;
                    //    responsemessage.emailID = UR.email; ;
                    //    responsemessage.Username = AUC.username;
                    //    responsemessage.BidAmountRs = BidAmountRs;
                    //    responsemessage.BidAmountUs = BidAmountUs;
                    //    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                    //    return response;
                    //}



                    // Add 3 Mins if bid closing time less than equal to 3 min

                    if (AUC.timeRemains <= 180 && AUC.timeRemains > 0)
                    {
                        CAS.UpdateBidClosingTime(AUC.productid);
                    }


                    // Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.priceus, 2).ToString());

                    listAuction = CAS.getBidRecordList(AUC);
                    AUC.userid = userid;
                    if (listAuction.Count > 0)
                    {
                        foreach (var record in listAuction)
                        {
                            record.recentbid = 0;
                            if (record.userid == AUC.userid)
                            {
                                record.currentbid = 0;
                            }
                            CAS.updateBid(record);
                        }
                    }

                    AUC.currentbid = 1;
                    AUC.recentbid = 0;
                    AUC.nickname = nickname;
                    AUC.username = username;
                    //string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    //if (string.IsNullOrEmpty(ipAddress))
                    //{
                    //    AUC.ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                    //}

                    if (browsercurrentbid != AUC.pricers)
                    {
                        responsemessage.currentStatus = 6;
                        responsemessage.msg = "Bid value has changed.Please try again";
                        responsemessage.mobileNum = "";
                        responsemessage.emailID = "";
                        responsemessage.Username = "";
                        responsemessage.BidAmountRs = 0;
                        responsemessage.BidAmountUs = 0;

                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                        return response;
                    }

                    AUC.Bidrecordid = CAS.InsertBidRecordMob(AUC, bidByVal, deviceTocken, OSversion, modelName, ipAddress, latitude, longitude, fullAddress, userLocation);

                    if (AUC.Bidrecordid == 0)
                    {
                        responsemessage.currentStatus = 8;
                        responsemessage.msg = "Bid closed";
                        responsemessage.mobileNum = "";
                        responsemessage.emailID = "";
                        responsemessage.Username = "";
                        responsemessage.BidAmountRs = 0;
                        responsemessage.BidAmountUs = 0;

                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                        return response;
                    }


                    // Update Bid Limit Function
                    if (isOldUser == 0)
                    {
                        //created on 28-1-20
                        #region
                        var currentLeadinguser = CAS.Getcurrentleadinguser(AUC.productid);
                        if (AUC.userid == currentLeadinguser.userid)
                        {
                            int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.Bidpricers;
                            US.UpdateBidLimit(currentLeadinguser.userid, updateLimit);
                            //Session["BidLimit"] = updateLimit;
                            int isleadcurrent = 1;
                            CAS.Isleading(AUC.productid, currentLeadinguser.userid, currentLeadinguser.Bidpricers, isleadcurrent);

                            Auction objAction = CAS.GetcurrentpriceuserAfterproxycheck(AUC.productid, AUC.userid);
                            if (objAction.ProxyAmt >= currentLeadinguser.Bidpricers)
                            {

                                if (AUC.userid != objAction.userid)
                                {


                                    int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    //Session["BidLimit"] = updateLimit1;
                                    int islessproxy = 0;     // use for 1 time when proxyamount out bid from current ammount
                                    CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                                }

                            }
                            else if (objAction.ProxyAmt < currentLeadinguser.Bidpricers && objAction.ProxyAmt != 0 && objAction.islessproxy == 0)
                            {

                                if (AUC.userid != objAction.userid)
                                {
                                    int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    //Session["BidLimit"] = updateLimit1;
                                    int islessproxy = 1;
                                    CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                                }

                            }

                            else
                            {
                                Auction objAction1 = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                                if (AUC.userid != objAction1.userid)
                                {
                                    var outbidpricers = CAS.getoutbidamount(AUC.productid, objAction1.userid);  // get outbid user

                                    int updateLimit1 = objAction1.amountlimt + outbidpricers;
                                    US.UpdateBidLimit(objAction1.userid, updateLimit1);

                                    int isleadoutbid = 0;
                                    CAS.Isleading(AUC.productid, objAction1.userid, outbidpricers, isleadoutbid);
                                }
                            }

                        }
                        else
                        {
                            //int returnamount = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                            //int updateLimit = amountlimt + returnamount;
                            //US.UpdateBidLimit(userid, updateLimit);
                            //Session["BidLimit"] = updateLimit;
                        }
                        #endregion
                        //int updateLimit = amountlimt - AUC.nextValidBidRs;
                        //US.UpdateBidLimit(userid, updateLimit);
                        //Session["BidLimit"] = updateLimit;
                    }
                    else
                    {
                        var currentLeadinguser = CAS.Getcurrentleadinguser(AUC.productid);

                        Auction objAction = CAS.GetcurrentpriceuserAfterproxycheck(AUC.productid, AUC.userid);
                        if (objAction.ProxyAmt >= currentLeadinguser.Bidpricers)
                        {

                            if (currentLeadinguser.userid != objAction.userid)
                            {


                                int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                //Session["BidLimit"] = updateLimit1;
                                int islessproxy = 0;     // use for 1 time when proxyamount out bid from current ammount
                                CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                int isleadoutbid = 0;
                                CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                            }

                        }
                        else if (objAction.ProxyAmt < currentLeadinguser.Bidpricers && objAction.ProxyAmt != 0 && objAction.islessproxy == 0)
                        {

                            if (currentLeadinguser.userid != objAction.userid)
                            {
                                int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                //Session["BidLimit"] = updateLimit1;
                                int islessproxy = 1;
                                CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                int isleadoutbid = 0;
                                CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                            }

                        }

                        else
                        {
                            Auction objAction1 = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                            if (currentLeadinguser.userid != objAction1.userid)
                            {
                                var outbidpricers = CAS.getoutbidamount(AUC.productid, objAction1.userid);  // get outbid user

                                int updateLimit1 = objAction1.amountlimt + outbidpricers;
                                US.UpdateBidLimit(objAction1.userid, updateLimit1);

                                int isleadoutbid = 0;
                                CAS.Isleading(AUC.productid, objAction1.userid, outbidpricers, isleadoutbid);
                            }
                        }
                    }

                    if (AUC.Bidrecordid > 0)
                    {
                        CAS.UpdateAcutionPrice(AUC);

                    }
                    else
                    {
                        //Failed
                    }

                    AUC.pricers = AUC.nextValidBidRs; // Current bid value 
                    AUC.priceus = AUC.nextValidBidUs;

                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                    AUC.nextValidBidUs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.nextValidBidUs, 2).ToString());

                    Auction returnAuc = COMC.AddProxybidRecords(AUC);

                    if (returnAuc != null)
                    {
                        // Update Bid Limit Function
                        if (returnAuc.isOldUser == 0)
                        {
                            //created on 03-02-20
                            #region
                            var currentLeadinguser = CAS.GetCurrentLeadingProxyuser(AUC.productid);


                            if (returnAuc.userid == currentLeadinguser.userid)
                            {

                                int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.ProxyAmt;
                                US.UpdateBidLimit(currentLeadinguser.userid, updateLimit);
                                //Session["BidLimit"] = updateLimit;
                                int isleadcurrent = 1;
                                CAS.Isleadingproxy(AUC.productid, currentLeadinguser.userid, currentLeadinguser.ProxyAmt, isleadcurrent);

                                Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, returnAuc.userid);

                                if (returnAuc.userid != objAction.userid)
                                {
                                    int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                                    if (checkforprocyuser > 0)
                                    {
                                        int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                        int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                        US.UpdateBidLimit(objAction.userid, updateLimit1);
                                        int isleadoutbid = 0;
                                        CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                                    }
                                    else
                                    {
                                        int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                        int updateLimit1 = objAction.amountlimt + outbidpricers;
                                        US.UpdateBidLimit(objAction.userid, updateLimit1);
                                        int isleadoutbid = 0;
                                        CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                                    }

                                }
                            }
                            else
                            {
                                //int returnamount = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                                //int updateLimit = amountlimt + returnamount;
                                //US.UpdateBidLimit(userid, updateLimit);
                                //Session["BidLimit"] = updateLimit;
                            }
                            #endregion
                            //int updateLimit = amountlimt - AUC.nextValidBidRs;
                            //US.UpdateBidLimit(userid, updateLimit);
                            //Session["BidLimit"] = updateLimit;
                        }

                        else
                        {
                            Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, returnAuc.userid);

                            if (returnAuc.userid != objAction.userid)
                            {
                                int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                                if (checkforprocyuser > 0)
                                {
                                    int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                                }
                                else
                                {
                                    int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidpricers;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                                }

                            }
                        }
                        if (AUC.userid != returnAuc.userid)
                        {
                            //Session["OutProxyErr"] = "have been out bid due to proxy. Kindly bid again.";
                            // CAll Mail Function 
                            //AUC.email = Session["emailId"].ToString();
                            //COMC.sendMailOutbid(AUC);

                            responsemessage.currentStatus = 2;
                            responsemessage.msg = "You have been out bid due to proxy. Kindly bid again.";
                            responsemessage.mobileNum = UR.Mobile;
                            responsemessage.emailID = UR.email;
                            responsemessage.Username = AUC.username;
                            responsemessage.BidAmountRs = BidAmountRs;
                            responsemessage.BidAmountUs = BidAmountUs;

                            response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                            return response;
                        }

                        responsemessage.currentStatus = 2;
                        responsemessage.msg = "You have been out bid due to proxy. Kindly bid again.";
                        responsemessage.mobileNum = UR.Mobile;
                        responsemessage.emailID = UR.email;
                        responsemessage.Username = AUC.username;
                        responsemessage.BidAmountRs = BidAmountRs;
                        responsemessage.BidAmountUs = BidAmountUs;

                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                        return response;
                    }
                    else
                    {
                        AUC.recentbid = 1;
                        CAS.updateRecentBid(AUC);
                    }
                    // Add to My Auction Gallary 
                    //addToGallary(auc);


                    //int isPresentArtist1 = CAS.CheckArtistAssginedbidUserlist(AUC);

                    var currentLeadinguseroutbid = CAS.Getcurrentleadinguser(AUC.productid);          // get current leading user for outbid  another
                    Auction objActionoutbid = CAS.Getcurrentoutbiduser(AUC.productid, currentLeadinguseroutbid.userid);    // get out bid user
                    if (currentLeadinguseroutbid.userid != objActionoutbid.userid)
                    {
                        //Session["OutProxyErr"] = "have been out bid due to proxy. Kindly bid again.";
                        // CAll Mail Function                      /// uncomment on 2/03/2020
                        if (objActionoutbid.userid > 0)
                        {
                            AUC.mailPreprice = objActionoutbid.Bidpricers;
                            AUC.curprice = currentLeadinguseroutbid.Bidpricers;
                            AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.curprice, 1).ToString());
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            //int page= Convert.ToInt32(Request["page"].ToString());

                            //created on 30_12_2020
                            COMC.sendMailOutbiduser(AUC, objActionoutbid.userid, objActionoutbid.name, objActionoutbid.lastname);                        
                            COMC.sendoutbidmsg(AUC.reference, objActionoutbid.name, objActionoutbid.mobile, objActionoutbid.lastname);

                            Pushnotification obj = new Pushnotification();
                            int user_id = objActionoutbid.userid;
                            string subject = "Out Bid";
                            string description = "Dear "+ objActionoutbid.name + " "+ objActionoutbid.lastname + " you have been outbid on Lot No." + AUC.reference.Trim() + ". To continue bidding for the lot, the Next valid bid is Rs." + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + "($" + Common.DollerFormat((AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"])).ToString()) + ")";
                            COMC.sendOutbidNotification(user_id, subject, description);
                            
                        }
                    
                    }

                    // Add to My Auction Gallary 
                    //int isPresentArtist1 = CAS.CheckArtistAssginedbidUserlist(auc);
                    //if (isPresentArtist1 > 0)
                    //{

                    //}
                    //else
                    //{
                    //    int addArtist = CAS.AddArtisttoBidUserList(auc);                   
                    //}

                    //addToGallary(AUC);

                    responsemessage.currentStatus = 3;
                    responsemessage.msg = "CURRENTLY LEADING";
                    responsemessage.mobileNum = UR.Mobile;
                    responsemessage.emailID = UR.email;
                    responsemessage.Username = AUC.username;
                    responsemessage.BidAmountRs = BidAmountRs;
                    responsemessage.BidAmountUs = BidAmountUs;                 
                    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                    return response;

                }
                else
                {
                    User UR = US.GetBillingAddress(userid);
                    responsemessage.currentStatus = 4;
                    responsemessage.msg = "User not exist please login again.";
                    responsemessage.mobileNum = "";
                    responsemessage.emailID = "";
                    responsemessage.Username = "";
                    responsemessage.BidAmountRs = 0;
                    responsemessage.BidAmountUs = 0;
                    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                    return response;

                }

            }
            catch (Exception Ex)
            {

                //Responsemessage responsemessage = new Responsemessage();
                responsemessage.currentStatus = 5;
                responsemessage.msg = "Something went wrong. Please try after sometime";
                responsemessage.mobileNum = "";
                responsemessage.emailID = "";
                responsemessage.Username = "";
                responsemessage.BidAmountRs = 0;
                responsemessage.BidAmountUs = 0;
                response = Request.CreateResponse(HttpStatusCode.BadRequest, responsemessage);
                return response;
            }

        }

    }
}
