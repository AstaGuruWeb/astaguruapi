﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class aboutUsController : ApiController
    {
        Utility util = new Utility();
        public resourceaboutUs Get()
        {

            List<aboutUs> objAuctionlist = new List<aboutUs>();

            DataTable dt = new DataTable();
            dt = util.Display("select aboutId,about,status from aboutUs");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    aboutUs objAuction = new aboutUs();
                    objAuction.aboutId = int.Parse(dr["aboutId"].ToString());
                    objAuction.about = dr["about"].ToString();
                    objAuction.status = int.Parse(dr["status"].ToString());



                    objAuctionlist.Add(objAuction);

                }

            }

            resourceaboutUs objresource = new resourceaboutUs();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
