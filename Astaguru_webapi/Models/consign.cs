﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru_webapi.Models
{
    public class consign
    {
        public string Sr { get; set; }
        public int Consignmentid { get; set; }

        public int Userid { get; set; }
        public string Artist { get; set; }

        public string Title { get; set; }

        public int categoryid { get; set; }

        //[Required(ErrorMessage = "Please Select Category")]
        public string category { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }

        public string Year { get; set; }

        public string Medium { get; set; }

        public string Height { get; set; }

        public string Width { get; set; }

        public string Depth { get; set; }

        public string Units { get; set; }

        public string EditionNumaber { get; set; }

        public string EditionSize { get; set; }

        public string WorkSign { get; set; }

        public string CertificateOfAuthencity { get; set; }

        public string AquireWork { get; set; }

        public string CityLocated { get; set; }

        public string Pricemind { get; set; }

        public string PriceCurrency { get; set; }

        public string Price { get; set; }

        public string MobileNumber { get; set; }

        public string UploadPhotos { get; set; }

        public string MoreInformation { get; set; }

        public List<string> MultiImage { get; set; }

        public List<consign> consignList { get; set; }

        public string Name { get; set; }

        public IList<consign> resource { get; set; }
    }

    public class ConsignResponse
    {
        public string Message { get; set; }

    }


    public class categoryconsign
    {
        public int categoryid { get; set; }
        public string category { get; set; }

    }


    public class resourceCategory
    {
        public List<categoryconsign> resource { get; set; }
    }

    

}