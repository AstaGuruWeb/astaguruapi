﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class UserRegister
    {

        public int userid { get; set; }

        public string firstname { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }


        public string lastname { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string state { get; set; }

        public string country { get; set; }
        public string telephone { get; set; }
        public string password { get; set; }
        public string fax { get; set; }
        public string BillingName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
        public string BillingZip { get; set; }
        public string BillingTelephone { get; set; }
        public string BillingEmail { get; set; }
    }

    public class Responseregister
    {
        public int userid { get; set; }
        public string message { get; set; }


    }
}

