﻿using Astaguru_webapi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace Astaguru_webapi.Controllers
{
    //[EnableCors(origins: "http://localhost:56311/", headers: "*", methods: "*")]
    [EnableCors(origins: "http://asta.kwebmakerdigital.com/", headers: "*", methods: "*")]
    public class MenuController : ApiController
    {
        // GET: api/Menu

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);

        
        public HttpResponseMessage Get()
        {
         
            var Menus = MenuRepository.Get();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Menus);
            return response;

        }

        // GET: api/Menu/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Menu
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Menu/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Menu/5
        public void Delete(int id)
        {
        }
    }
}
