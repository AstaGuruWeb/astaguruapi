﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class SendEmailController : ApiController
    {
        HttpResponseMessage response;
        Utility util = new Utility();

        public resposendmail Post(SendMail objsend)
        {
            bool result = false;
            #region Send Mail
            try
            {
                string EmailUserName = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string EmailPassword = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                string EmailHost = ConfigurationManager.AppSettings["smtpHost"].ToString();
                string EmailPort = ConfigurationManager.AppSettings["smtpPort"].ToString();
                bool EnableSsl = true;
                //bool EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"].ToString());
                string subject = objsend.subject;

                string ToEmailid = string.Empty;

                foreach (var objuser in objsend.to)
                {
                    ToEmailid= objuser.email;
                }
                string toEmail = ToEmailid;
                string body = objsend.body_text;


                System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(objsend.from_email, "Astaguru");
                // Recipient e-mail address.
                Msg.To.Add(toEmail);
                Msg.Subject = subject;
                Msg.Body = body;
                Msg.IsBodyHtml = true;


                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();

                smtp.Host = EmailHost;

                smtp.Port = int.Parse(EmailPort);
                // smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(EmailUserName, EmailPassword);
                //smtp.Credentials = new System.Net.NetworkCredential(objsend.from_email, "");

                smtp.EnableSsl = EnableSsl;
                //smtp.UseDefaultCredentials = false;

                smtp.Send(Msg);
                // strSuccess = "Success";

                result = true; //success
            }
            catch (Exception ex)
            {

                result = false; //failed
                //  ViewBag.Error = "Try again after some time.";
                ///return View();
            }

            if (result == true)
            {
                int count = 1;

                List<resposendmail> objlist = new List<resposendmail>();
                resposendmail objresposendmail = new resposendmail();

                objresposendmail.count = count;

                //objlist.Add(objresposendmail);

                //response = Request.CreateResponse(HttpStatusCode.OK, objlist);
                return objresposendmail;
            }
            else
            {
                int count = 0;
                List<resposendmail> objlist = new List<resposendmail>();
                resposendmail objresposendmail = new resposendmail();

                objresposendmail.count = count;

                //objlist.Add(objresposendmail);
                //response = Request.CreateResponse(HttpStatusCode.OK, objlist);
                return objresposendmail;
            }
            #endregion
          
        }
     }
}
