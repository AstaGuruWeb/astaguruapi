﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class AuctionProductdetailsController : ApiController
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        //HttpResponseMessage response;
        public AuctionProductdetails Get(int productid)
        {
            //List<AuctionProductdetails> objaucList = new List<AuctionProductdetails>();

            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter("select *,ar.FirstName,ar.LastName,ar.artistofthemonth,ar.Title as artistTitle,cat.category,auc.PrVat as newPrVat,md.medium from acution auc inner join category cat on auc.categoryid = cat.categoryid inner join medium md on auc.mediumid = md.mediumid inner join artist ar on ar.artistid = auc.artistid where auc.productid = " + productid + "", con);
            ad.Fill(dt);

            AuctionProductdetails objAuction = new AuctionProductdetails();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //AuctionProductdetails objAuction = new AuctionProductdetails();
                    objAuction.productid = int.Parse(dr["productid"].ToString());
                    objAuction.title = dr["title"].ToString();
                    objAuction.description = dr["description"].ToString();
                    objAuction.artistid = int.Parse(dr["artistid"].ToString());
                    objAuction.pricers = int.Parse(dr["pricers"].ToString());
                    objAuction.priceus = int.Parse(dr["priceus"].ToString());
                    objAuction.categoryid = int.Parse(dr["categoryid"].ToString());
                    objAuction.styleid = int.Parse(dr["styleid"].ToString());
                    //objAuction.mediumid = int.Parse(dr["mediumid"].ToString());
                    objAuction.featured = dr["featured"].ToString();
                    objAuction.collectors = dr["collectors"].ToString();
                    objAuction.thumbnail = dr["thumbnail"].ToString();
                    objAuction.image = dr["image"].ToString();
                    objAuction.productsize = dr["productsize"].ToString();
                    objAuction.timecount = int.Parse(dr["timecount"].ToString());
                    objAuction.productdate = dr["productdate"].ToString();
                    objAuction.reference = dr["reference"].ToString();
                    objAuction.proxy = dr["proxy"].ToString();
                    objAuction.proxyamount = int.Parse(dr["proxyamount"].ToString());
                    objAuction.proxyuserid = int.Parse(dr["proxyuserid"].ToString());
                    objAuction.Pproxyusername = dr["Pproxyusername"].ToString();
                  
                    //objAuction.timeRemains = int.Parse(dr["timeRemains"].ToString());


                    DateTime newbidclosetime = Convert.ToDateTime(dr["Bidclosingtime"].ToString());
                    objAuction.Bidclosingtime = string.Format("{0:yyyy-MM-dd hh:mm:ss}", newbidclosetime);

                    //DateTime tempcurrent = Convert.ToDateTime(dr["currentDate"].ToString());
                    //objAuction.currentDate = string.Format("{0:yyyy-MM-dd hh:mm:ss}", tempcurrent);


                    objAuction.Online = dr["Online"].ToString();

                    objAuction.estamiate = dr["estamiate"].ToString();
                    objAuction.smallimage = dr["smallimage"].ToString();               
                    if (dr["pricelow"].ToString() == "")
                    {
                        objAuction.pricelow = 0;
                    }
                    else
                    {
                        objAuction.pricelow = int.Parse(dr["pricelow"].ToString());
                    }

                    if (dr["pricehigh"].ToString() == "")
                    {
                        objAuction.pricehigh = 0;
                    }
                    else
                    {
                        objAuction.pricehigh = int.Parse(dr["pricehigh"].ToString());
                    }
                    objAuction.active = dr["active"].ToString();
                    objAuction.Prdescription = dr["Prdescription"].ToString();
                    objAuction.PrVat = dr["newPrVat"].ToString();
                    objAuction.HumanFigure = dr["HumanFigure"].ToString();
                    if (dr["Threshhold"].ToString() == null || dr["Threshhold"].ToString() == "")
                    {
                        objAuction.Threshhold = 0;
                    }
                    else
                    {
                        objAuction.Threshhold = int.Parse(dr["Threshhold"].ToString());
                    }


                    objAuction.Abrasion = dr["Abrasion"].ToString();
                    objAuction.Blistering = dr["Blistering"].ToString();
                    objAuction.Damage = dr["Damage"].ToString();
                    objAuction.Cupping = dr["Cupping"].ToString();
                    objAuction.Discoloration = dr["Discoloration"].ToString();
                    objAuction.Deterioration = dr["Deterioration"].ToString();
                    objAuction.Cracking = dr["Cracking"].ToString();
                    objAuction.Scratches = dr["Scratches"].ToString();
                    objAuction.Fungus = dr["Fungus"].ToString();
                    objAuction.Restoration = dr["Restoration"].ToString();
                    objAuction.ConditionDetails = dr["ConditionDetails"].ToString();
                    objAuction.Flaking = dr["Flaking"].ToString();
                    objAuction.Crease = dr["Crease"].ToString();
                    
                    //objAuction.LastName = dr["LastName"].ToString();
                    //objAuction.Picture = dr["Picture"].ToString();
                    //objAuction.Profile = dr["Profile"].ToString();
                    //objAuction.category = dr["category"].ToString();
                    //objAuction.style = dr["style"].ToString();
                    //objAuction.medium = dr["medium"].ToString();
                    //objAuction.Auctionname = dr["Auctionname"].ToString();
                    //objAuction.auctionBanner = dr["auctionBanner"].ToString();
                    //objAuction.DollarRate = double.Parse(dr["DollarRate"].ToString());

                    //objAuction.auctionType = int.Parse(dr["auctionType"].ToString());
                    objAuction.isInternational = int.Parse(dr["isInternational"].ToString());
                    objAuction.isInternationalGST = int.Parse(dr["isInternationalGST"].ToString());
                    objAuction.astaguruPrice = int.Parse(dr["astaguruPrice"].ToString());
                    
                    if (dr["usedGoodPercentage"].ToString() == null || dr["usedGoodPercentage"].ToString() == "")
                    {
                        objAuction.usedGoodPercentage = 0;
                    }
                    else
                    {
                        objAuction.usedGoodPercentage = int.Parse(dr["usedGoodPercentage"].ToString());
                    }

                    if (dr["nonExportable"].ToString() == "")
                    {
                        objAuction.nonExportable = 0;
                    }
                    else
                    {
                        objAuction.nonExportable = int.Parse(dr["nonExportable"].ToString());
                    }

                    objAuction.view1= dr["view1"].ToString();
                    objAuction.view2 = dr["view2"].ToString();
                    objAuction.view3 = dr["view3"].ToString();
                    objAuction.view4 = dr["view4"].ToString();

                    objAuction.isMargin = int.Parse(dr["isMargin"].ToString());
                    objAuction.listImage= dr["listImage"].ToString();

                    Artist objartist = new Artist();
                    List<Artist> ArtistList = new List<Artist>();
                    objartist.artistid= int.Parse(dr["artistid"].ToString());
                    objartist.FirstName = dr["FirstName"].ToString();
                    objartist.LastName = dr["LastName"].ToString();
                    objartist.artistofthemonth= dr["artistofthemonth"].ToString();
                    objartist.Picture = dr["Picture"].ToString();
                    objartist.Profile = dr["Profile"].ToString();
                    objartist.Title = dr["artistTitle"].ToString();
                    //ArtistList.Add(objartist);
                    


                    Category objcat = new Category();
                    List<Category> CategoryList = new List<Category>();
                    objcat.categoryid = int.Parse(dr["categoryid"].ToString());
                    objcat.category = dr["category"].ToString();



                    objcat.PrVat = dr["newPrVat"].ToString().Trim();

                    //CategoryList.Add(objcat);

                    Medium objmedium = new Medium();
                    List<Medium> MediumList = new List<Medium>();
                    objmedium.mediumid = int.Parse(dr["mediumid"].ToString());
                    objmedium.medium = dr["medium"].ToString();
                    //MediumList.Add(objmedium);




                    //objAuction.artist_by_artistid = ArtistList; 
                    //objAuction.category_by_categoryid = CategoryList;
                    //objAuction.medium_by_mediumid = MediumList;

                    objAuction.artist_by_artistid = objartist;
                    objAuction.category_by_categoryid = objcat;
                    objAuction.medium_by_mediumid = objmedium;



                    //objaucList.Add(objAuction);

                }
            }


            return objAuction;

    


        }


    }
}
