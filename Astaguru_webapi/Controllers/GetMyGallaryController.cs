﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class GetMyGallaryController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;


        public resourcedata Get(int userid,string status)
        {
            List<CurrentAuction> objAuctionlist = new List<CurrentAuction>();

            DataTable dt = new DataTable();

            dt = util.Display("exec Proc_getMyGallery 'getMyGalleryMob'," + userid + ",'" + status + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    CurrentAuction objAuction = new CurrentAuction();
                    objAuction.productid = int.Parse(dr["productid"].ToString());
                    objAuction.title = dr["title"].ToString();
                    objAuction.description = dr["description"].ToString();
                    objAuction.artistid = int.Parse(dr["artistid"].ToString());
                    //objAuction.Bidpricers = int.Parse(dr["Bidpricers"].ToString());
                    //objAuction.Bidpriceus = int.Parse(dr["Bidpriceus"].ToString());
                    objAuction.Bidpricers = int.Parse(dr["pricers"].ToString());
                    objAuction.Bidpriceus = int.Parse(dr["priceus"].ToString());
                    objAuction.categoryid = int.Parse(dr["categoryid"].ToString());
                    objAuction.styleid = int.Parse(dr["styleid"].ToString());
                    objAuction.mediumid = int.Parse(dr["mediumid"].ToString());
                    objAuction.featured = dr["featured"].ToString();
                    objAuction.collectors = dr["collectors"].ToString();
                    objAuction.thumbnail = dr["thumbnail"].ToString();
                    objAuction.image = dr["image"].ToString();
                    objAuction.productsize = dr["productsize"].ToString();
                    objAuction.timecount = int.Parse(dr["timecount"].ToString());
                    objAuction.productdate = dr["productdate"].ToString();
                    objAuction.reference = dr["reference"].ToString();
                    objAuction.proxy = dr["proxy"].ToString();
                    objAuction.proxyamount = int.Parse(dr["proxyamount"].ToString());
                    objAuction.proxyuserid = int.Parse(dr["proxyuserid"].ToString());
                    objAuction.Pproxyusername = dr["Pproxyusername"].ToString();
                    objAuction.myBidClosingTime = dr["myBidClosingTime"].ToString();
                    objAuction.timeRemains = int.Parse(dr["timeRemains"].ToString());


                    //DateTime newbidclosetime = Convert.ToDateTime(dr["Bidclosingtime"].ToString());
                    //objAuction.Bidclosingtime = string.Format("{0:yyyy-MM-dd hh:mm:ss}", newbidclosetime);

                    //DateTime tempcurrent = Convert.ToDateTime(dr["currentDate"].ToString());
                    //objAuction.currentDate = string.Format("{0:yyyy-MM-dd hh:mm:ss}", tempcurrent);

                    objAuction.Bidclosingtime = string.Format("{0:yyyy-MM-dd hh:mm:ss}", dr["Bidclosingtime"].ToString());


                    objAuction.currentDate = string.Format("{0:yyyy-MM-dd hh:mm:ss}", dr["currentDate"].ToString());


                    objAuction.Online = dr["Online"].ToString();
                    objAuction.estamiate = dr["estamiate"].ToString();
                    objAuction.smallimage = dr["smallimage"].ToString();
                    if (dr["pricelow"].ToString() == "")
                    {
                        objAuction.pricelow = 0;
                    }
                    else
                    {
                        objAuction.pricelow = int.Parse(dr["pricelow"].ToString());
                    }

                    if (dr["pricehigh"].ToString() == "")
                    {
                        objAuction.pricehigh = 0;
                    }
                    else
                    {
                        objAuction.pricehigh = int.Parse(dr["pricehigh"].ToString());
                    }
                    objAuction.active = dr["active"].ToString();
                    objAuction.Prdescription = dr["Prdescription"].ToString();
                    objAuction.PrVat = dr["PrVat"].ToString();
                    objAuction.HumanFigure = dr["HumanFigure"].ToString();
                    if (dr["Threshhold"].ToString() == null || dr["Threshhold"].ToString() == "")
                    {
                        objAuction.Threshhold = 0;
                    }
                    else
                    {
                        objAuction.Threshhold = int.Parse(dr["Threshhold"].ToString());
                    }


                    objAuction.Abrasion = dr["Abrasion"].ToString();
                    objAuction.Blistering = dr["Blistering"].ToString();
                    objAuction.Damage = dr["Damage"].ToString();
                    objAuction.Cupping = dr["Cupping"].ToString();
                    objAuction.Discoloration = dr["Discoloration"].ToString();
                    objAuction.Deterioration = dr["Deterioration"].ToString();
                    objAuction.Cracking = dr["Cracking"].ToString();
                    objAuction.Scratches = dr["Scratches"].ToString();
                    objAuction.Fungus = dr["Fungus"].ToString();
                    objAuction.Restoration = dr["Restoration"].ToString();
                    objAuction.ConditionDetails = dr["ConditionDetails"].ToString();
                    objAuction.Flaking = dr["Flaking"].ToString();
                    objAuction.Crease = dr["Crease"].ToString();
                    objAuction.FirstName = dr["FirstName"].ToString();
                    objAuction.Myreference = int.Parse(dr["Myreference"].ToString());
                    objAuction.LastName = dr["LastName"].ToString();
                    objAuction.Picture = dr["Picture"].ToString();
                    objAuction.Profile = dr["Profile"].ToString();
                    objAuction.category = dr["category"].ToString();
                    objAuction.style = dr["style"].ToString();
                    objAuction.medium = dr["medium"].ToString();
                    objAuction.Auctionname = dr["Auctionname"].ToString();

                    //objAuction.DollarRate = int.Parse(dr["DollarRate1"].ToString());
                    objAuction.DollarRate = int.Parse(dr["DollarRate"].ToString());

                    if (dr["MyUserID"].ToString() == null || dr["MyUserID"].ToString() == "")
                    {
                        objAuction.MyUserID = 0;
                    }
                    else
                    {
                        objAuction.MyUserID = int.Parse(dr["MyUserID"].ToString());
                    }

                    objAuction.auctionType = int.Parse(dr["auctionType"].ToString());
                    objAuction.isInternational = int.Parse(dr["isInternational"].ToString());
                    objAuction.isInternationalGST = int.Parse(dr["isInternationalGST"].ToString());
                    objAuction.astaguruPrice = int.Parse(dr["astaguruPrice"].ToString());
                    if (dr["usedGoodPercentage"].ToString() == null || dr["usedGoodPercentage"].ToString() == "")
                    {
                        objAuction.usedGoodPercentage = 0;
                    }
                    else
                    {
                        objAuction.usedGoodPercentage = int.Parse(dr["usedGoodPercentage"].ToString());
                    }

                    objAuction.nonExportable = int.Parse(dr["nonExportable"].ToString());


                    objAuction.bidartistuserid = dr["bidartistuserid"].ToString();

                    objAuction.status = dr["status"].ToString();

                    //if (dr["Ownerid"].ToString() == "")
                    //{
                    //    objAuction.Ownerid = 0;
                    //}
                    //else
                    //{
                    //    objAuction.Ownerid = int.Parse(dr["Ownerid"].ToString());
                    //}



                    objAuctionlist.Add(objAuction);

                }

            }

            resourcedata objresource = new resourcedata();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;
        }
    }
}
