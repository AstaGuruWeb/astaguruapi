﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;


namespace Astaguru_webapi.Controllers
{
    public class SendNotificationController : ApiController
    {

        HttpResponseMessage response;
        Responsemessage responsemessage = new Responsemessage();
        public HttpResponseMessage Post(Pushnotification obj)
        {
            try
            {
                var applicationID = "AAAAa-wlbPw:APA91bEBK31iJ8EdA6cElzIxaPfyTgfS77poQUuIkc1qbJezwifX1Q6hnLjED6IycmhyM5eZGwPdh-XJCvUSX8-UcOCLfd-rB_oThZhQ8_sZqOOTOOqSsGsdMjCGFA5-DgxGvUGlHc9j";

                //var senderId = "57-------55";

                string deviceId = obj.device_id;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    to = deviceId,

                    notification = new

                    {

                        body = obj.description,

                        title = obj.subject,

                        //shortdesc = shortdescription
                        //icon = "myicon"

                    }
                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);


                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;
                                responsemessage.msg = "Notification Send Successfully";
                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                                return response;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, str);
                return response;

            }
        }
    }
}
