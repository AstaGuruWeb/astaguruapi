﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class PriceFormatController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;
        public HttpResponseMessage Get(string Pricers, string Priceus)
        {
            PriceFormate Myobj = new PriceFormate();
            List<PriceFormate> objAuctionlist = new List<PriceFormate>();

            Myobj.FormatPricers = Astaguru_webapi.Services.Common.rupeeFormat(Pricers);
            Myobj.FormatPriceus = Astaguru_webapi.Services.Common.DollerFormat(Priceus);

            objAuctionlist.Add(Myobj);


            response = Request.CreateResponse(HttpStatusCode.OK, objAuctionlist);
            return response;

        }
    }
}
