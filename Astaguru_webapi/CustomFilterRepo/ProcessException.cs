﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.CustomFilterRepo
{
    public class ProcessException : Exception
    {
        public ProcessException(string message)
            : base(message)
        {

        }
    }
}