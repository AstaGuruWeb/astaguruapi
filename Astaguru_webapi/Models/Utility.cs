﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using WebGrease.Activities;
using Repository.Lib;


namespace Astaguru_webapi.Models
{
    public class Utility
    {
        Log log = new Log();

        #region Connection
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        #endregion

        #region Execute Query
        public bool Execute(SqlCommand cmd)
        {
            try
            {

                con.Open();
                cmd.Connection = con;
                int i = cmd.ExecuteNonQuery();
                return (i > 0);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return false;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Table by command
        public DataTable Display(SqlCommand cmd) //using overloading methord
        {
            try
            {
                con.Open();
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd); //get data into adapter

                DataTable dt = new DataTable();
                da.Fill(dt);    //store into table
                return dt;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Table by query
        public DataTable Display(string query) //using overloading methord
        {
            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

                DataTable dt = new DataTable();
                da.Fill(dt);    //store into table

                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Set by command
        public DataSet Display1(SqlCommand cmd) //using overloading methord
        {
            try
            {
                con.Open();
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd); //get data into adapter

                DataSet ds = new DataSet();
                da.Fill(ds);    //store into table
                return ds;
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Set by query
        public DataSet Display1(string query) //using overloading methord
        {
            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

                DataSet ds = new DataSet();
                da.Fill(ds);    //store into table

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        #endregion


        #region Send Email
        public bool SendEmail(string Emailbody, string[] Toemailids, string Subject, string FilePath, Stream input)
        {
            bool result = false;
            #region Send Mail
            try
            {
                string EmailUserName = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string EmailPassword = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                string EmailHost = ConfigurationManager.AppSettings["smtpHost"].ToString();
                string EmailPort = ConfigurationManager.AppSettings["smtpPort"].ToString();
                //bool EnableSsl = false;
                bool EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"].ToString());
                string subject = Subject;

                string ToEmailid = string.Empty;
                for (int i = 0; i < Toemailids.Length; i++)
                {
                    ToEmailid += Toemailids[i] + ",";
                }
                if (ToEmailid != "")
                {
                    ToEmailid = ToEmailid.Substring(0, ToEmailid.Length - 1);
                }

                string toEmail = ToEmailid;
                //string body = Emailbody;

                string str = string.Empty;
                str = "<table width='650' align='center' cellpadding='0' cellspacing='0' style='font-family: 'Montserrat', sans-serif; line-height:24px ; font-size:14px; font-weight:normal; border:1px solid #ebebeb;'>";
                str += "<tr bgcolor='#282831'>";
                str += "<td width='220' style='padding:20px 0 20px 20px'><img src='https://www.astaguru.com/content/images/Email/logo.png' width='175' height='44' /></td>";
                str += "<td colspan='2'>";
                str += "<a href='https://www.astaguru.com/LiveAuction' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Live Auctions</a>";
                str += "<a href='https://www.astaguru.com/UpcomingAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Upcoming Auctions</a>";
                str += "<a href='https://www.astaguru.com/PastAuctions' style='color:#fff;text-decoration:none; text-transform:capitalize; margin:0 10px;'>Past Auctions</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td colspan='3' style='position:relative;'><a href=\"https://astaguru.com/ContactUs\" target='_blank'><img src='https://www.astaguru.com/content/images/Email/banner_ContactUs.jpg'  width='650'/></a> ";
                //str += "<a href=\"" + basePathURL + "/Home/emailVerification?activationcode=" + UL.activationcode + "\" target='_blank' style='background:#c1aa5c; color:#fff; border:1px solid #fff; border-radius:3px; position:absolute; text-decoration: none; font-size: 15px; font-weight: normal;  text-align: center; padding: 5px 10px; left: 38%; bottom: 35px; transform:all .3s ease-in;'>Verify your Account</a>";
                str += "</td>";
                str += "</tr>";
                str += "<tr>";
                str += "<td colspan='3' style='padding:30px;'>";
                str += "<p style='text-align:justify;'>" + Emailbody + "</p>";
                str += "</td>";
                str += "</tr> ";
                str += "<tr  bgcolor='#ebebeb' >";
                str += "<td  width='220' style='padding:20px 0 20px 30px'><img src='https://www.astaguru.com/content/images/Email/logo_black.png' width='122' height='31' /></td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Follow Us</span></p>";
                str += "<a href='https://www.facebook.com/Astaguru-Auction-House-375561749218131/?fref=ts' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/facebook_icon.png'  title='Facebook' /></a>";
                str += "<a href='https://in.pinterest.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/pinterest_icon.png' title='Pinterest' /></a>";
                str += "<a href='https://twitter.com/astagurutweets' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/twitter_icon.png'  title='Twitter' /></a>";
                str += "<a href='https://www.instagram.com/astaguru/' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/instagram_icon.png'  title='Instagram' /></a>";
                str += "<a href='https://www.youtube.com/channel/UCmTqSUMAHV5l0mACoK72t7g' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/youtube_icon.png'  title='Youtube' /></a>";
                str += "</td>";
                str += "<td align='center'>";
                str += "<p style='font-size:12px; text-align:center; margin-bottom:5px; '><span style='border-bottom:1px solid #000;'>Download Our App</span></p>";
                str += "<a href='https://play.google.com/store/apps/details?id=com.astaguru&hl=en' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/google_play.png'  title='Google Play' /></a>";
                str += "<a href='https://apps.apple.com/app/id1483891832' target='_blank' style='margin:0 2px;'><img src='https://www.astaguru.com/content/images/Email/app_store.png'  title='App Store' /></a>";
                str += "</td>";
                str += "</tr> ";
                str += "</table>";


                System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(EmailUserName, "AstaGuru");
                // Recipient e-mail address.
                Msg.To.Add(toEmail);
                Msg.Subject = subject;
                Msg.Body = str;
                Msg.IsBodyHtml = true;

                //file upload
                if (FilePath != "")
                {
                    Attachment attach = new Attachment(input, FilePath);
                    Msg.Attachments.Add(attach);
                }


                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();

                smtp.Host = EmailHost;

                smtp.Port = int.Parse(EmailPort);
                // smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(EmailUserName, EmailPassword);

                smtp.EnableSsl = EnableSsl;
                //smtp.UseDefaultCredentials = false;

                smtp.Send(Msg);
                // strSuccess = "Success";

                result = true; //success
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                result = false; //failed
                //  ViewBag.Error = "Try again after some time.";
                ///return View();
            }

            return result;
            #endregion
        }
        #endregion


        public bool IsValidImageFileExtension(string extension)
        {
            return (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".bmp");
        }


    }
}