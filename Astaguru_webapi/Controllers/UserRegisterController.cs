﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class UserRegisterController : ApiController
    {
        HttpResponseMessage response;
        Utility util = new Utility();

        public HttpResponseMessage Post(UserRegister objuser)
        {
            Responseregister responsemessage = new Responseregister();
            try
            {
                DataTable dt = new DataTable();

                using (SqlCommand cmd = new SqlCommand("CRUDUSER"))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Mode", "InsertRegistrationMobile");
                    cmd.Parameters.AddWithValue("@name", objuser.name);
                    cmd.Parameters.AddWithValue("@Mobile",objuser.mobile);
                    cmd.Parameters.AddWithValue("@email",objuser.email);
                    cmd.Parameters.AddWithValue("@Username", objuser.firstname);
                    cmd.Parameters.AddWithValue("@lastname",objuser.lastname);
                    cmd.Parameters.AddWithValue("@address1",objuser.address);
                    cmd.Parameters.AddWithValue("@city",objuser.city);
                    cmd.Parameters.AddWithValue("@zip",objuser.zip);
                    cmd.Parameters.AddWithValue("@state",objuser.state);
                    cmd.Parameters.AddWithValue("@country",objuser.country);
                    cmd.Parameters.AddWithValue("@telephone", objuser.telephone);
                    cmd.Parameters.AddWithValue("@password",objuser.password);
                    cmd.Parameters.AddWithValue("@fax", objuser.fax);
                    cmd.Parameters.AddWithValue("@Billingname", objuser.BillingName);
                    cmd.Parameters.AddWithValue("@BillingAddress",objuser.BillingAddress);
                    cmd.Parameters.AddWithValue("@BillingCity", objuser.BillingCity);
                    cmd.Parameters.AddWithValue("@BillingState", objuser.BillingState);
                    cmd.Parameters.AddWithValue("@BillingCountry",objuser.BillingCountry);
                    cmd.Parameters.AddWithValue("@BillingZip", objuser.BillingZip);
                    cmd.Parameters.AddWithValue("@BillingTelephone", objuser.BillingTelephone);
                    cmd.Parameters.AddWithValue("@BillingEmail", objuser.BillingEmail);





                    if (util.Execute(cmd))
                    {
                        int userid=0;
                        dt = util.Display("select userid from users where password='"+ objuser.password + "' and email='"+ objuser.email + "' and name='"+ objuser.name + "'");

                        if (dt.Rows.Count > 0)
                        {
                             userid = int.Parse(dt.Rows[0]["userid"].ToString());
                        }

                        

                        responsemessage.userid = userid;
                        responsemessage.message = "User Successfully Register";                        
                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
         
                    }
                    else
                    {
                        responsemessage.message = "User not Successfully Register";
                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                    }
                }

                return response;
            }
            catch (Exception Ex)
            {
                responsemessage.message = "Something went wrong. Please try after sometime";
                response = Request.CreateResponse(HttpStatusCode.BadRequest, responsemessage);
                return response;
            }
        }
        }
}
