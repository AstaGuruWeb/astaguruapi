﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class SaveConsignController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;


        public HttpResponseMessage Post(consign objconsign)
        {
            ConsignResponse responsemessage = new ConsignResponse();
            foreach (var obj in objconsign.resource)
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("Proc_Consignment"))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@para", "add");
                        cmd.Parameters.AddWithValue("@Artist_Designer", obj.Artist);
                        cmd.Parameters.AddWithValue("@Title", obj.Title);
                        cmd.Parameters.AddWithValue("@category", obj.category);
                        cmd.Parameters.AddWithValue("@Year", obj.Year);
                        cmd.Parameters.AddWithValue("@Medium", obj.Medium);
                        cmd.Parameters.AddWithValue("@Height", obj.Height);
                        cmd.Parameters.AddWithValue("@Width", obj.Width);
                        cmd.Parameters.AddWithValue("@Depth", obj.Depth);
                        cmd.Parameters.AddWithValue("@Units", obj.Units);
                        cmd.Parameters.AddWithValue("@EditionNumaber", obj.EditionNumaber);
                        cmd.Parameters.AddWithValue("@EditionSize", obj.EditionSize);
                        cmd.Parameters.AddWithValue("@WorkSign", obj.WorkSign);
                        cmd.Parameters.AddWithValue("@CertificateOfAuthencity", obj.CertificateOfAuthencity);
                        cmd.Parameters.AddWithValue("@AquireWork", obj.AquireWork);
                        cmd.Parameters.AddWithValue("@CityLocated", obj.CityLocated);
                        cmd.Parameters.AddWithValue("@Pricemind", obj.Pricemind);
                        cmd.Parameters.AddWithValue("@PriceCurrency", obj.PriceCurrency);
                        cmd.Parameters.AddWithValue("@Price", obj.Price);
                        cmd.Parameters.AddWithValue("@MobileNumber", obj.MobileNumber);
                        //cmd.Parameters.AddWithValue("@UploadPhotos", objconsign.UploadPhotos);
                        cmd.Parameters.AddWithValue("@MoreInformation", obj.MoreInformation);
                        cmd.Parameters.AddWithValue("@Userid", obj.Userid);

                        cmd.Parameters.Add("@Consignmentid", SqlDbType.Int).Direction = ParameterDirection.Output;

                        if (util.Execute(cmd))
                        {
                            string id = cmd.Parameters["@Consignmentid"].Value.ToString();
                            obj.Consignmentid = int.Parse(id);

                            if (obj.MultiImage != null)
                            {
                                //List<string> objImgList = TempData["Mutlti"] as List<string>;

                                foreach (var list in obj.MultiImage)
                                {
                                    using (SqlCommand cmd1 = new SqlCommand("Proc_ConsignMultipleImage"))
                                    {
                                        cmd1.CommandType = CommandType.StoredProcedure;
                                        cmd1.Parameters.AddWithValue("@Para", "Add");
                                        cmd1.Parameters.AddWithValue("@Consignmentid", obj.Consignmentid);
                                        cmd1.Parameters.AddWithValue("@ImageName", "data:image/jpeg;base64,"+list);
                                        if (util.Execute(cmd1))
                                        {
                                            responsemessage.Message = "Data Save Successfully";
                                            response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);

                                        }
                                    }
                                }
                                responsemessage.Message = "Data Save Successfully";
                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                                //response = Request.CreateResponse(HttpStatusCode.OK, "Saved Successfully");
                            }
                            else
                            {
                                responsemessage.Message = "Data Save Successfully";
                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                            }



                        }
                        else
                        {
                            responsemessage.Message = "Data Not Save Successfully";
                            response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                        }

                    }
                }
                catch (Exception ex)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, ex.Message);
                }
            }
         
                return response;
        }
   }
}
