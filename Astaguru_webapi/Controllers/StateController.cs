﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class StateController : ApiController
    {

        Utility util = new Utility();
        public resourcestate Get(int countryid)
        {

            List<state> objAuctionlist = new List<state>();

            DataTable dt = new DataTable();
            dt = util.Display("select countryid,stateid,statename from state where countryid=" + countryid + "");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    state objAuction = new state();
                    objAuction.countryid = int.Parse(dr["countryid"].ToString());
                    objAuction.stateid = int.Parse(dr["stateid"].ToString());
                    objAuction.statename = dr["statename"].ToString();


                    objAuctionlist.Add(objAuction);

                }

            }

            resourcestate objresource = new resourcestate();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
