﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class GetNotificationDetailController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;

        public HttpResponseMessage Get(int NotificationID)
        {
            List<NotificationDetail> objnotificationlist = new List<NotificationDetail>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_PushNotification 'GetNotificationDetail',0,'','',''," + NotificationID + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    NotificationDetail objnotification = new NotificationDetail();
                    objnotification.NotificationID = dr["NotificationID"].ToString();
                    objnotification.UserID = dr["userid"].ToString();
                    objnotification.FirstName = dr["FirstName"].ToString();
                    objnotification.LastName = dr["LastName"].ToString();
                    objnotification.IsRead = dr["IsRead"].ToString();
                    objnotification.NotificationBody = dr["description"].ToString();
                    objnotification.CreatedDate = dr["createdOn"].ToString();
                    objnotification.msg = dr["msg"].ToString();


                    objnotificationlist.Add(objnotification);

                }
            }
            response = Request.CreateResponse(HttpStatusCode.OK, objnotificationlist);
            return response;

        }
    }
}
