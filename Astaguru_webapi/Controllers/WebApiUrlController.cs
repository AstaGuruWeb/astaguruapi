﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class WebApiUrlController : ApiController
    {
        Utility util = new Utility();
        public resourceWebApiUrl Get()
        {

            List<WebApiUrl> objAuctionlist = new List<WebApiUrl>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_WebApiUrl 'Get'");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    WebApiUrl objurl = new WebApiUrl();
                    objurl.id = int.Parse(dr["Id"].ToString());
                    objurl.mode = dr["Mode"].ToString();
                    objurl.apiurl = dr["apiurl"].ToString();



                    objAuctionlist.Add(objurl);

                }

            }

            resourceWebApiUrl objresource = new resourceWebApiUrl();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
