﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class WebApiUrl
    {
        public int id { get; set; }

        public string mode { get; set; }

        public string apiurl { get; set; }
    }

    public class resourceWebApiUrl
    {
        public List<WebApiUrl> resource { get; set; }
    }
}