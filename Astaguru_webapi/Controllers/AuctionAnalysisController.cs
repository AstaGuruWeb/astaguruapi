﻿using Astaguru_webapi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Repository.Lib;
namespace Astaguru_webapi.Controllers
{
    public class AuctionAnalysisController : ApiController
    {
        Utility util = new Utility();
        Log log = new Log();
        HttpResponseMessage response = new HttpResponseMessage();

        public HttpResponseMessage Get(int AuctionId)
        {



            List<AuctionAnalysis> objaucList = new List<AuctionAnalysis>();

           
            List<AuctionforAuctionAnalysis> objauctionanalisList = new List<AuctionforAuctionAnalysis>();


            List<AuctionListAuctionAnalysis> objlist = new List<AuctionListAuctionAnalysis>();

            ArrayList list = new ArrayList();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec spGetAuctionAnalysis " + AuctionId + "");


            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AuctionListAuctionAnalysis obj = new AuctionListAuctionAnalysis();
                    obj.Auctionname = dr["Auctionname"].ToString();
                    obj.Date = dr["Date"].ToString();
                    obj.DollarRate = dr["DollarRate"].ToString();

                    objlist.Add(obj);

                }
            }



            if (ds.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow dr1 in ds.Tables[1].Rows)
                {
                    AuctionAnalysis objAuction = new AuctionAnalysis();
                    objAuction.auctionAnalysisID = dr1["auctionAnalysisID"].ToString();
                    objAuction.auctionTitle = dr1["auctionTitle"].ToString();
                    objAuction.numberOfLots = dr1["numberOfLots"].ToString();
                    objAuction.lotPercentage = dr1["lotPercentage"].ToString();
                    objAuction.winningValueRs = dr1["winningValueRs"].ToString();
                    objAuction.winingValueUs = dr1["winingValueUs"].ToString();
                    objAuction.buyersPreminum = dr1["buyersPreminum"].ToString();
                    objAuction.typeFlg = dr1["typeFlg"].ToString();
                    objaucList.Add(objAuction);


                }
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                foreach (DataRow dr2 in ds.Tables[2].Rows)
                {

                    AuctionforAuctionAnalysis objauctionanalis = new AuctionforAuctionAnalysis();
                    objauctionanalis.productid = dr2["productid"].ToString();
                    objauctionanalis.title = dr2["title"].ToString();
                    objauctionanalis.pricers = dr2["pricers"].ToString();
                    objauctionanalis.priceus = dr2["priceus"].ToString();
                    objauctionanalis.FirstName = dr2["FirstName"].ToString();
                    objauctionanalis.LastName = dr2["LastName"].ToString();
                    objauctionanalisList.Add(objauctionanalis);

                }
            }





            list.Add(objlist);
            list.Add(objaucList);
            list.Add(objauctionanalisList);









            response = Request.CreateResponse(HttpStatusCode.OK, list);
            return response;





        }
    }
}
