﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class ArtistincurrentauctionByAuctionIdController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;
        public resourceArtist Get(string auctionType,int AuctionID)
        {
            List<Artist> objaucList = new List<Artist>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec spGetArtistincurrentauction '"+ auctionType + "',"+ AuctionID + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Artist objAuction = new Artist();

                    objAuction.FirstName = dr["FirstName"].ToString();
                    objAuction.LastName = dr["LastName"].ToString();
                    objAuction.artistid = int.Parse(dr["artistid"].ToString());


                    objaucList.Add(objAuction);

                }
            }
            resourceArtist objresource = new resourceArtist();
            objresource.resource = objaucList;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
