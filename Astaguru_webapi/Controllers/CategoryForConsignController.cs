﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Astaguru_webapi.Controllers
{
    public class CategoryForConsignController : ApiController
    {
        Utility util = new Utility();
        public resourceCategory Get()
        {

            //List<Country> objAuctionlist = new List<Country>();

            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_video 'Getcategoryforvideo'");
            List<categoryconsign> objList = new List<categoryconsign>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    categoryconsign obj = new categoryconsign();

                    obj.categoryid = int.Parse(dr["categoryid"].ToString());
                    obj.category = dr["category"].ToString();

                    objList.Add(obj);

                }

            }

            resourceCategory objresource = new resourceCategory();
            objresource.resource = objList;
            return objresource;
        }
    }
}
